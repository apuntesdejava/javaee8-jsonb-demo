# Un vistazo a JSON-B de Java EE8 #

Java EE 8 viene con muchas características interesantes, y en este post veremos un poco de la nueva implementación: JSON Binding, o también conocido como JSON-B (JSR-367)

El post completo se encuentra en [Un vistazo a JSON-B de Java EE8](http://www.apuntesdejava.com/2017/04/un-vistazo-json-b-de-java-ee8.html)