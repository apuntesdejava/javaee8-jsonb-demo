package com.apuntesdejava.javaee8.jsonb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;

/**
 *
 * @author diego
 */
public class Main {

    public static void main(String[] args) {
        Jsonb jsonb = JsonbBuilder.create();
        //Convirtiendo objeto a JSON
        Persona p = new Persona(1, "Diego");
        String obj2json = jsonb.toJson(p);
        System.out.println("json:" + obj2json);

        //Convirtiendo JSON a Objeto
        String json = "{\"id\":10, \"nombre\":\"java\", \"fechaNacimiento\":\"1976-03-27T00:00:00\", \"activo\":true }";
        Persona q = jsonb.fromJson(json, Persona.class);
        System.out.println("objeto:" + q);

        //Convirtiendo Lista a JSON
        List<Persona> lista = Arrays.asList(p, q);
        String listaStr = jsonb.toJson(lista);
        System.out.println("Lista json:" + listaStr);

        //Convirtiendo JSON a Lista
        String listaJson = "[{\"id\":1,\"nombre\":\"Ana\"},{\"id\":2,\"nombre\":\"Beto\",\"activo\":true},{\"id\":3,\"nombre\":\"Carlos\",\"fechaNacimiento\":\"2000-05-06T00:00:00\"}]";
        List<Persona> personas = jsonb.fromJson(listaJson, new ArrayList<Persona>() {
        }.getClass());
        System.out.println("Personas:" + personas);

        //Personalizado el JSONP
        JsonbConfig config = new JsonbConfig()
                .withFormatting(Boolean.TRUE)
                .withDateFormat("dd/MM/yyyy", Locale.getDefault())
                .withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_DASHES);

        String json2 = "{\"id\":10,\"fecha_nacimiento\":\"23/08/1999\" }";
        Jsonb jsonb2 = JsonbBuilder.create(config);
        Persona p2 = jsonb2.fromJson(json2, Persona.class);
        System.out.println("json personalizado:" + p2);
        String json3 = jsonb2.toJson(q);
        System.out.println("persona con json personalizado:" + json3);
    }
}
